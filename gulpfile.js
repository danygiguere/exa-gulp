var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefix = require('gulp-autoprefixer');
var csso = require('gulp-csso');

gulp.task('style', function () {
    return gulp.src('src/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefix({
            browsers: ['last 2 versions']
        }))
        .pipe(csso())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('.tmp'))
        .pipe(gulp.dest('dist/css'))
});

gulp.task('html', function () {
    return gulp.src('src/index.html')
        .pipe(gulp.dest('.tmp'))
        .pipe(gulp.dest('dist'))
});

gulp.task('default', ['style', 'html']);
